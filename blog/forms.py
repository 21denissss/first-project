from django.contrib.auth.models import User
from django import forms
from .models import Post, Comment, Profile
 
class PostForm(forms.ModelForm):
    category = forms.ChoiceField(choices=(), required=True)
    def __init__(self, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)
        self.fields['category'].choices = (('Пушкин','Пушкин'),('Лермонтов','Лермонтов'),('Маяковский','Маяковский'))
    class Meta: 
        model = Post
        fields = ('title', 'text', 'category')
        
class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ('author', 'text',)
		
class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Repeat password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'email')

    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('Passwords don\'t match.')
        return cd['password2']

class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ()
		
class FilterForm(forms.Form):
    surname = forms.CharField(label='Surname', required=False)
    
class NewFilterForm(forms.Form):
    category = forms.ChoiceField(choices=(('Все','Все'),('Пушкин','Пушкин'),('Лермонтов','Лермонтов'),('Маяковский','Маяковский')), required=True)